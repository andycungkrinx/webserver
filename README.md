## Webserver
```sh
Dockerize webserver static for html base on alpine
```

## Feature
```sh
- Traefik 2.4
- Nginx 1.20.0 (include pagespeed, brotli and modsec module)
- Icluded template bootsrap
```

## How to use
```sh
- ./run.sh (start all container)
- ./stop.sh (stop all container)
```

